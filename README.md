**Get random sample of devices**

Uses cache feature of NAPI. Gets 100 random devices that are TLML2, deployed, and less than the latest firmware version and prints to screen.
from opslib import napi
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', default=100, help='Number of Random units to pass back')
parser.add_argument('-fw', default=4.126, help='The firmware revision devices need to be below to be eligible')
args = parser.parse_args()

eligible = []
notEligible = []
sample = int(args.s)
latest = float(args.fw)

lteDevices = napi.get('/devices/search?item_number=TLML2').json()

for device in lteDevices:
    cacheData = napi.get_device_meta(device)#, update=True if you want logs to update
    deviceData = cacheData['device']
    deviceStatus = deviceData['status']
    deviceFw = deviceData['sw_rev']
    if len(deviceFw)>5:
        #weird devices with unsupported firmware numbers
        print 'this is weird: %s, %s' % (device, deviceFw)
    else:
        deviceFw = float(deviceFw)
    if deviceStatus == 'deployed' and deviceFw < latest:
        shortDevice = deviceData['shortDeviceId']
        eligible.append(shortDevice)
    else:
        notEligible.append((device, deviceStatus, deviceFw))

print 'Eligible Devices Total: ', len(eligible)
np.random.shuffle(eligible)
selectedDevices = eligible[:sample]
print selectedDevices
